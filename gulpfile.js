/*
 * import modules
 *
 */

var nib = require('nib'),
    gulp = require('gulp'),
    stylus = require('gulp-stylus'),
    rimraf = require('gulp-rimraf'),
    concat = require('gulp-concat'),
    minifyCss = require('gulp-minify-css'),
    sourcemaps = require('gulp-sourcemaps');

/*
 * config
 *
 */

var config = {
    stylusMain: './catalog/view/theme/default/stylesheet/main.styl',
    CSSSource: './catalog/view/theme/default/stylesheet/',
    CSSDest:     './catalog/view/theme/default/stylesheet/',
    jsSource:       '',
    jsDest:          ''
}

var stylusMainFile = '';

gulp.task('styles-source-map', function () {
  gulp.src(config.stylusMain)
    .pipe(stylus({'include css': true, use: [nib()]}))
    .pipe(sourcemaps.init())
    .pipe(minifyCss())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest(config.CSSDest));
});

gulp.task('styles-no-source-map', function () {
  gulp.src(config.stylusMain)
    .pipe(stylus({'include css': true, use: [nib()]}))
    .pipe(minifyCss())
    .pipe(gulp.dest(config.CSSDest));
});

gulp.task('clear', function () {
  return gulp.src('./system/cache/**/*', {read: false})
    .pipe(rimraf());
});


/*
 * watches
 *
 */

gulp.task('watch', function () {
  gulp.watch(config.CSSSource+'**/*', ['styles-source-map']);
});


 /*
 * global tasks
 *
 */

gulp.task('default', ['styles-source-map', 'watch']);
gulp.task('prod', ['styles-no-source-map']);
